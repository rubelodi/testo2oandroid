package com.rld.mo2o.test.repository

import junit.framework.Assert.assertTrue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

/**
 * Integration test of API Calls
 *  - Check if the Parse Model is correct
 *  - Check if the Implementation of Retrofit and EndPoint is correct
 *  - Check if the the repository methods is correct
 */
@ExperimentalCoroutinesApi
internal class BeerRepositoryTest {

    private lateinit var repo: BeerRepository

    @Before
    fun init() {
        repo = BeerRepository.getInstance()
    }

    @Test
    fun requestBeer() = runBlocking {
        var result = repo.requestBeer(1, 5)
        assertTrue(result.isNotEmpty())
    }

    @Test
    fun testRequestBeer() = runBlocking {
        var result1 =  repo.requestBeer("Buzz", 1, 5)
        assertTrue(result1.isNotEmpty())

        var result2 = repo.requestBeer("Alpha Dog", 1, 5)
        assertTrue(result2.isNotEmpty())
    }
}