package com.rld.mo2o.test.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rld.mo2o.test.R
import com.rld.mo2o.test.databinding.BeerCardItemBinding
import com.rld.mo2o.test.diffutil.BeerDiffUtil
import com.rld.mo2o.test.model.Beer
import com.rld.mo2o.test.ui.listbeer.ListBeerFragmentDirections
import com.rld.mo2o.test.ui.listbeer.ListBeerViewModel
import kotlinx.android.synthetic.main.beer_card_item.view.*

class BeerListAdapter (val viewModel: ListBeerViewModel) : ListAdapter<Beer, BeerListAdapter.ViewHolder>(BeerDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.beer_card_item, parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
        if (position % 3 == 0 && itemCount - position < 5) viewModel.requestNext()
    }

    inner class ViewHolder(val binding: BeerCardItemBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            val extras = FragmentNavigatorExtras(
                binding.root.imageView to "sharedView"
            )
            binding.setClickListener {
                it.findNavController().navigate(ListBeerFragmentDirections.actionListBeerFragmentToDetailBeer(binding.beer!!),extras)
            }
        }


        fun bind(item: Beer) {
            binding.beer = item
            binding.executePendingBindings()
        }
    }
}