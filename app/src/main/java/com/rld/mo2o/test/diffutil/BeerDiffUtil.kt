package com.rld.mo2o.test.diffutil

import androidx.recyclerview.widget.DiffUtil
import com.rld.mo2o.test.model.Beer


class BeerDiffUtil : DiffUtil.ItemCallback<Beer>() {
    override fun areItemsTheSame(oldItem: Beer, newItem: Beer) = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Beer, newItem: Beer) = oldItem.id == newItem.id
}