package com.rld.mo2o.test.model
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class BoilVolume (

	@SerializedName("value") val value : Double,
	@SerializedName("unit") val unit : String
) : Serializable