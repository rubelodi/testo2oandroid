package com.rld.mo2o.test.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Hops (

	@SerializedName("name") val name : String,
	@SerializedName("amount") val amount : Amount,
	@SerializedName("add") val add : String,
	@SerializedName("attribute") val attribute : String
) : Serializable