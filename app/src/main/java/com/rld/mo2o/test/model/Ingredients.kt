package com.rld.mo2o.test.model
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class Ingredients (

	@SerializedName("malt") val malt : List<Malt>,
	@SerializedName("hops") val hops : List<Hops>,
	@SerializedName("yeast") val yeast : String
) : Serializable