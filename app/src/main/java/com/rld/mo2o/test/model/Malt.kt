package com.rld.mo2o.test.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class Malt (

	@SerializedName("name") val name : String,
	@SerializedName("amount") val amount : Amount
) : Serializable