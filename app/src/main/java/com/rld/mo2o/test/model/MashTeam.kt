package com.rld.mo2o.test.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class MashTeam (

	@SerializedName("temp") val temp : Temp,
	@SerializedName("duration") val duration : Double
) : Serializable