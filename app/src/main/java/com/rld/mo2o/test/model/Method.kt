package com.rld.mo2o.test.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Method (

    @SerializedName("mash_temp") val mash_temp : List<MashTeam>,
    @SerializedName("fermentation") val fermentation : Fermentation,
    @SerializedName("twist") val twist : String
) : Serializable