package com.rld.mo2o.test.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Temp (

	@SerializedName("value") val value : Int,
	@SerializedName("unit") val unit : String
) : Serializable