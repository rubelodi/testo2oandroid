package com.rld.mo2o.test.repository

import com.rld.mo2o.test.rest.Rest
import com.rld.mo2o.test.rest.RestInterface

class BeerRepository private constructor( private val apiClient: RestInterface) {

    suspend fun requestBeer (name : String, page: Int, elements: Int) = apiClient.getBeer(name.replace(" ", "_"),page, elements)

    suspend fun requestBeer (page: Int, elements: Int) = apiClient.getBeer(page, elements)

    companion object {
        @Volatile private var instance: BeerRepository? = null

        fun getInstance() =
            instance ?: synchronized(this) {
                instance ?: BeerRepository( Rest.getClient()).also { instance = it }
            }
    }
}