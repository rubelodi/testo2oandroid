package com.rld.mo2o.test.rest

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class Rest {

    companion object {
        private const val REQUEST_TIMEOUT = 20

        private var retrofit: Retrofit? = null
        private var okHttpClient: OkHttpClient? = null

        private val GSON: Gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create()

        private var api: RestInterface? = null

        fun getClient(): RestInterface {

            initOkHttp()

            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl("https://api.punkapi.com/v2/")
                    .client(okHttpClient)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(GSON))
                    .build()

                api = retrofit!!.create(RestInterface::class.java)
            }
            return api!!
        }

        private fun initOkHttp() {
            val httpClient = OkHttpClient().newBuilder()
                .connectTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
                .readTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
                .writeTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)

            val interceptor = HttpLoggingInterceptor()

            interceptor.level = HttpLoggingInterceptor.Level.BODY

            httpClient.addInterceptor(interceptor)
            okHttpClient = httpClient.build()
        }
    }
}