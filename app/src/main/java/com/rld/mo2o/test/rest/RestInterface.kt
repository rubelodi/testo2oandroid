package com.rld.mo2o.test.rest


import com.rld.mo2o.test.model.Beer
import retrofit2.http.*

interface RestInterface {

    @GET("beers")
    suspend fun getBeer(@Query("beer_name") name : String, @Query("page") page: Int, @Query("per_page") elements: Int): List<Beer>

    @GET("beers")
    suspend fun getBeer(@Query("page") page: Int, @Query("per_page") elements: Int): List<Beer>
}