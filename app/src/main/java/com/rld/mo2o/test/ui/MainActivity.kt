package com.rld.mo2o.test.ui

import android.graphics.PorterDuff
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.rld.mo2o.test.R
import com.rld.mo2o.test.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.view.*
import java.util.Observer

class MainActivity : AppCompatActivity() {

     lateinit var navController: NavController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = setupDataBinding()
        setupNavigation()
        setupToolbar(binding)
    }

    private fun setupDataBinding(): ActivityMainBinding {
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        return binding
    }

    private fun setupNavigation() {
        navController = Navigation.findNavController(this,
            R.id.nav_host_fragment
        )
    }

    private fun setupToolbar(binding: ActivityMainBinding) {
        setSupportActionBar(binding.root.toolbar)
        NavigationUI.setupActionBarWithNavController(this, navController)
    }

    override fun onSupportNavigateUp() = Navigation.findNavController(this, R.id.nav_host_fragment).navigateUp()

}
