package com.rld.mo2o.test.ui.detailbeer

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.transition.TransitionInflater
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavArgs
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.NavigationUI

import com.rld.mo2o.test.R
import com.rld.mo2o.test.databinding.DetailBeerFragmentBinding
import com.rld.mo2o.test.model.Beer
import com.rld.mo2o.test.ui.MainActivity
import kotlinx.android.synthetic.main.detail_beer_fragment.*
import kotlinx.android.synthetic.main.detail_beer_fragment.view.*

class DetailBeer : Fragment() {

    private lateinit var binding: DetailBeerFragmentBinding


    private val args : DetailBeerArgs by navArgs()

    private val viewModel: DetailBeerViewModel by lazy {
        DetailBeerViewModel(args.beer)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<DetailBeerFragmentBinding>(inflater, R.layout.detail_beer_fragment, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            beer = viewModel.beer
        }
        return binding.root
    }






}
