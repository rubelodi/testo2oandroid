package com.rld.mo2o.test.ui.listbeer

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

import com.rld.mo2o.test.R
import com.rld.mo2o.test.adapters.BeerListAdapter
import com.rld.mo2o.test.databinding.ListBeerFragmentBinding
import kotlinx.android.synthetic.main.list_beer_fragment.view.*

class ListBeerFragment : Fragment() {

    lateinit var binding: ListBeerFragmentBinding

    private val listBearViewModel: ListBeerViewModel by lazy {
        ViewModelProvider(requireActivity()).get(ListBeerViewModel::class.java)
    }

    private val adapter by lazy {
        BeerListAdapter(listBearViewModel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<ListBeerFragmentBinding>(inflater, R.layout.list_beer_fragment, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = listBearViewModel
        }

        binding.root.rcvBeer.adapter = adapter
        subscribeUi()
        return binding.root
    }


    private fun subscribeUi() {
        listBearViewModel.items.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })

        listBearViewModel.searchText.observe(viewLifecycleOwner, Observer {
            listBearViewModel.requestInitial()
        })
    }

}
