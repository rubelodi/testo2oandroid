package com.rld.mo2o.test.ui.listbeer

import android.view.View
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rld.mo2o.test.model.Beer
import com.rld.mo2o.test.repository.BeerRepository
import kotlinx.coroutines.*

class ListBeerViewModel : ViewModel() {
    val searchText: MutableLiveData<String> = MutableLiveData("")
    val items = MutableLiveData<List<Beer>>()
    val empty = MediatorLiveData<Boolean>()
    val showProgress = MutableLiveData(false)


    val repo = BeerRepository.getInstance()

    var page = 1

    init {
        empty.addSource(items) {
            viewModelScope.launch {
                if (it.isEmpty()) delay(500)
                empty.value = it.isEmpty()
            }
        }
    }

    fun requestInitial() {
        showProgress.value = true
        page = 1
        GlobalScope.launch(Dispatchers.IO) {
            var response  =  if(searchText.value!!.isNullOrEmpty()){
                repo.requestBeer(page, 5 )
            }else{
                repo.requestBeer(searchText.value!!,page, 5)
            }
            items.postValue(response)

            withContext(Dispatchers.Main){
                showProgress.value = false
            }

        }
    }

    fun requestNext(){
        showProgress.value = true
        GlobalScope.launch(Dispatchers.IO) {
            var response = if(searchText.value!!.isNullOrEmpty()){
                repo.requestBeer(++page, 5)
            }else{
                repo.requestBeer(searchText.value!!,++page, 5)
            }
            val temp = items.value!!.toMutableList()
            temp.addAll(response)
            items.postValue(temp)
            withContext(Dispatchers.Main){
                showProgress.value = false
            }
        }
    }

}
